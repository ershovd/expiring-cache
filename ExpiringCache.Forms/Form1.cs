﻿using ExpiringCache.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExpiringCache.Forms
{
    public partial class MainForm : Form
    {
        private ExpiringCache<Guid, string> _cache;
        private System.Timers.Timer _updateUITimer;
        private TimeSpan _duration = TimeSpan.FromSeconds(20);
        private TimeSpan _timerInterval = TimeSpan.FromSeconds(1);
        private int _maxCap = 10;

        public MainForm()
        {
            InitializeComponent();

            _cache = new ExpiringCache<Guid, string>(_duration, _timerInterval, _maxCap);

            _updateUITimer = new System.Timers.Timer();
            _updateUITimer.Interval = TimeSpan.FromMilliseconds(100).TotalMilliseconds;
            _updateUITimer.Elapsed += OnUpdateTimer;
            _updateUITimer.Start();


            lblDuration.Text = _duration.TotalSeconds.ToString();
            lblTimerInterval.Text = _timerInterval.TotalSeconds.ToString();
        }

        private void OnUpdateTimer(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (lblItemCount.InvokeRequired)
            {
                lblItemCount.Invoke(new Action(() => lblItemCount.Text = _cache.Count.ToString()));
            }
            
        }

        private void btnAddItem_Click(object sender, EventArgs e)
        {
            _cache.Add(Guid.NewGuid(), "test1");
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            _updateUITimer.Elapsed -= OnUpdateTimer;
            _updateUITimer.Stop();

            _cache.Dispose();
        }
    }
}
