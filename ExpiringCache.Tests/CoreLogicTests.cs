﻿using System;
using ExpiringCache.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ExpiringCache.Tests
{
    [TestClass]
    public class CoreLogicTests
    {
        IExpiringCache<Guid, string> _cache;

        [TestInitialize]
        public void Initialize()
        {
            _cache = new ExpiringCache<Guid, string>();
        }

        [TestCleanup]
        public void DeInit()
        {
            _cache.Dispose();
        }

        [TestMethod]
        public void BasicAddAndTryGet()
        {
            var guid1 = Guid.NewGuid();
            _cache.Add(guid1, "test1", TimeSpan.FromSeconds(20));
            _cache.Add(Guid.NewGuid(), "test2", TimeSpan.FromSeconds(20));
            _cache.Add(Guid.NewGuid(), "test3", TimeSpan.FromSeconds(20));

            string val;
            var result = _cache.TryGet(guid1, out val);

            Assert.AreEqual(val, "test1");
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void AddWithUpdate()
        {
            var guid1 = Guid.NewGuid();
            _cache.Add(guid1, "test1", TimeSpan.FromSeconds(20));
            _cache.Add(guid1, "test2", TimeSpan.FromSeconds(20));


            string val;
            var result = _cache.TryGet(guid1, out val);

            Assert.AreEqual(val, "test2");
            Assert.IsTrue(result);
        }


        [TestMethod]
        public void RemoveIfExceedingMaxCap()
        {
            var newCache = new ExpiringCache<Guid, string>(TimeSpan.FromSeconds(20), TimeSpan.FromSeconds(1), 3);
            var guid1 = Guid.NewGuid();
            var guid2 = Guid.NewGuid();
            var guid3 = Guid.NewGuid();
            var guid4 = Guid.NewGuid();


            newCache.Add(guid1, "test1");
            newCache.Add(guid2, "test2");
            newCache.Add(guid3, "test3");
            newCache.Add(guid4, "test4");


            var res1 = newCache.TryGet(guid1, out string s);
            Assert.IsFalse(res1);

            string s2;
            var res2 = newCache.TryGet(guid2, out s2);
            Assert.IsTrue(res2);
            Assert.AreEqual(s2, "test2");

            string s3;
            var res3 = newCache.TryGet(guid3, out s3);
            Assert.IsTrue(res3);
            Assert.AreEqual(s3, "test3");

            string s4;
            var res4 = newCache.TryGet(guid4, out s4);
            Assert.IsTrue(res4);
            Assert.AreEqual(s4, "test4");

        }

    }
}
