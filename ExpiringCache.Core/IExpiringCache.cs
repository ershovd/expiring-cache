﻿using System;

namespace ExpiringCache.Core
{
    /// <summary>
    /// Thread safe cache with limiting numbers of items and where elements are removed if not accessed.
    /// </summary>
    /// <typeparam name="TKey">Type of key</typeparam>
    /// <typeparam name="TItem">Type of data item</typeparam>    
    public interface IExpiringCache<TKey, TItem> : IDisposable
    {
        /// <summary>
        /// Adds (or updates) an element with default duration
        /// </summary>
        void Add(TKey key, TItem item);
        
        /// <summary>
        /// Adds (or updates) an element with a specific duration overriding the default duration.
        /// </summary>
        void Add(TKey key, TItem item, TimeSpan duration);
        
        /// <summary>
        /// Gets an element with a specific key and resets its last-accessed property
        /// </summary>
        bool TryGet(TKey key, out TItem item);
    }
}