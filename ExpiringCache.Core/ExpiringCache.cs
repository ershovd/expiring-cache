﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Timers;

namespace ExpiringCache.Core
{
    /// <summary>
    /// Default implementation of the IExpiringCache s
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    /// <typeparam name="TItem"></typeparam>
    public class ExpiringCache<TKey, TItem> : IExpiringCache<TKey, TItem>
    {

        /// <summary>
        /// Default capacity of collection
        /// </summary>
        private static readonly int DefaultMaxCapacity = 100;
        /// <summary>
        /// Default lifetime duration of the element
        /// </summary>
        private static readonly TimeSpan DefaultDuration = TimeSpan.FromMinutes(1);
        /// <summary>
        /// Default timer interval
        /// </summary>
        private static readonly TimeSpan DefaultTimerInterval = TimeSpan.FromSeconds(1);

        private readonly TimeSpan m_Duration;
        private readonly int m_MaxCapacity;
        private readonly TimeSpan m_TimerInterval;
        private readonly Object m_LockObject = new Object();
        private Timer m_Timer;
        private Dictionary<TKey, TimeStampItemHolder> m_Items;

        public ExpiringCache() : this(DefaultDuration, DefaultTimerInterval, DefaultMaxCapacity)
        {
        }

        public ExpiringCache(TimeSpan duration, TimeSpan timerInterval, int maxCapacity)
        {
            m_Duration = duration;
            m_TimerInterval = timerInterval;
            m_MaxCapacity = maxCapacity;
            m_Timer = new Timer();
            m_Timer.Interval = timerInterval.TotalMilliseconds;
            m_Timer.Elapsed += OnTimerCheck;
            m_Timer.AutoReset = false;
            m_Timer.Start();

            m_Items = new Dictionary<TKey, TimeStampItemHolder>();
        }

        /// <summary>
        /// Returns ticks of current time (adjusted to UTC)
        /// </summary>        
        private long GenerateTimeStamp()
        {
            return DateTime.UtcNow.Ticks;
        }

        private void OnTimerCheck(object sender, ElapsedEventArgs e)
        {
            RemoveExpiredElements();
            m_Timer.Start();
        }

        /// <summary>
        /// Remove items in the list if they exceed duration
        /// </summary>
        private void RemoveExpiredElements()
        {
            if (Count == 0)
            {
                return;
            }

            var currentTime = GenerateTimeStamp();
            var expiredItems = m_Items.Where(f => f.Value.IsExpired(currentTime)).ToList();
            if (expiredItems.Any())
            {
                lock (m_LockObject)
                {
                    // Make sure to consider only items that are still expired after the lock was taken
                    expiredItems = expiredItems.Where(f => f.Value.IsExpired(currentTime)).ToList();
                    foreach (var item in expiredItems)
                    {
                        // Remove only if the same object is still stored in the cache
                        if (m_Items.TryGetValue(item.Key, out var colletionItem))
                        {
                            if (item.Value == colletionItem)
                            {
                                m_Items.Remove(item.Key);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Remove latest by access time if capacity exceeded
        /// </summary>
        private void RemoveExcessByAccessTime()
        {
            if (Count == 0)
            {
                return;
            }

            lock (m_LockObject)
            {
                if (m_Items.Count > m_MaxCapacity)
                {
                    var keyList = m_Items
                        .OrderByDescending(f => f.Value.LastAccessed)
                        .Skip(m_MaxCapacity)
                        .Select(f => f.Key)
                        .ToList();
                    RemoveFromItems(keyList);
                }
            }
        }

        private void RemoveFromItems(IList<TKey> keys)
        {
            foreach (var key in keys)
            {
                m_Items.Remove(key);
            }
        }

        public void Add(TKey key, TItem item)
        {
            Add(key, item, m_Duration);
        }

        public void Add(TKey key, TItem item, TimeSpan duration)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }

            var currentTime = GenerateTimeStamp();

            lock (m_LockObject)
            {
                if (m_Items.TryGetValue(key, out var holder))
                {
                    holder.LastAccessed = currentTime;
                    holder.Item = item;
                }
                else
                {
                    var newItem = new TimeStampItemHolder(item, duration, currentTime);
                    m_Items.Add(key, newItem);

                }
            }

            RemoveExcessByAccessTime();
        }

        public bool TryGet(TKey key, out TItem item)
        {
            var currentTime = GenerateTimeStamp();

            lock (m_LockObject)
            {
                if (m_Items.TryGetValue(key, out var holder))
                {
                    holder.LastAccessed = currentTime;
                    item = holder.Item;
                    return true;
                }
            }
            item = default;
            return false;
        }

        /// <summary>
        /// Count of elements
        /// </summary>
        public int Count
        {
            get
            {
                lock (m_LockObject)
                {
                    return m_Items?.Count ?? 0;
                }
            }
        }

        #region Dispose pattern implementation

        bool m_Disposed = false;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (m_Disposed)
            {
                return;
            }

            if (disposing)
            {
                lock (m_LockObject)
                {
                    m_Items.Clear();
                }

                m_Timer.Stop();
                m_Timer.Elapsed -= OnTimerCheck;
                m_Timer.Dispose();

                // Free any other managed objects here.
                //
            }

            // Free any unmanaged objects here.
            //
            m_Disposed = true;
        }

        ~ExpiringCache()
        {
            Dispose(false);
        }

        #endregion


        /// <summary>
        /// Utility holder of the data item, duration and creation timestamp.
        /// </summary>
        private class TimeStampItemHolder
        {
            public TimeStampItemHolder(TItem item, TimeSpan duration, long ticks)
            {
                this.Item = item;
                this.Duration = duration;
                this.LastAccessed = ticks;
            }

            public TItem Item { get; set; }
            public long LastAccessed { get; set; }
            public TimeSpan Duration { get; private set; }

            public override string ToString()
            {
                return $" Last-Access: {TimeSpan.FromTicks(LastAccessed).TotalSeconds}; Duration (sec): {Duration.TotalSeconds}";
            }

            /// <summary>
            /// True if element has exceed current time ticks
            /// </summary>
            public bool IsExpired(long currentTimeTicks)
            {
                return (currentTimeTicks - LastAccessed) > Duration.Ticks;
            }
        }

    }

}
